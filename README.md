# Jeu de la vie

Le jeu de la vie est un automate cellulaire imaginé par John Conway. Cette application en est une implémentation <i>web</i> permettant de créer des univers du jeu de la vie. Vous pouvez y importer des fichiers, les modifier et les jouer en direct et exporter vos modifications ainsi que personnaliser vos règles de naissance–survie et la vitesse de jeu.

## Installation

Ce projet repose sur un environnement [<cite>Node.js</cite>](https://nodejs.org/en/download/package-manager/all) et nécessite le gestionnaire de paquets <cite>NPM</cite>. L’utilisation de [<cite>nvm</cite>](https://github.com/nvm-sh/nvm) est recommandée.

Il vous faudra aussi la chaine d’outils de [<cite>Rust</cite>](https://www.rust-lang.org/tools/install) ainsi que [<cite>Wasm Pack</cite>](https://rustwasm.github.io/wasm-pack/installer/) pour compiler vers la cible WASM.

Pour activer les crochets du projet, exécutez `make install-hooks` après avoir cloné le projet.

## Démarrer

### Développement

Commencez par installer les dépendances avec `npm install` (ou `npm ci`).

Dans un environnement de développement, démarrez le serveur avec une des commandes suivantes via :

- en développement (avec HMR) : `npm run dev`
- en recette : `npm run build && npm run preview`

Et rendez‐vous sur votre navigateur à l’adresse indiquée dans le terminal ou vérifiez le statut avec `curl -I localhost:<PORT>`.

Recompilez le code <cite>Rust</cite> avec `make wasm` après avoir effectué des modifications.

### Production

Utilisez <cite>Docker Compose</cite> pour créer un environnement de production, notamment via `docker compose up -d`. Vous pouvez modifier les variables via un fichier `.env` ou en les spécifiant avant votre commande `PORT=8080 docker [...]`.

## Support

N’hésitez pas à ouvrir une <i>issue</i> pour obtenir de l’aide, soumettre des suggestions ou signaler des bogues. Vous pouvez me contacter à l’adresse florian.monfort@proton.me.

## Feuille de route

- Ajouter des contrôles de composants
- Ajouter des contrôles de bout en bout avec <cite>Cypress</cite> ou <cite>Playwright</cite>

## Contribuer

Vous pouvez librement proposer des changements dans le code en publiant des demandes de fusion.

Vous devez valider votre code avec les configurations <cite>ESLint</cite> et <cite>TypeScript</cite> fournies.

## Auteurs et contributeurs

Principalement écrit par Florian Monfort.

## License

Cette application est sous licence GPLv3.
