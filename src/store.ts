import { persisted } from "svelte-persisted-store"
import { derived, writable } from "svelte/store"

import { DEFAULT_ZOOM_LEVEL } from "src/constants"
import type { Coordinates } from "src/types"

import EditMode from "src/enums/EditMode"
import type Modal from "src/enums/Modal"
import PlayStrategy from "src/enums/PlayStrategy"
import ScrollBehavior from "src/enums/ScrollBehavior"
import TapSlideBehavior from "src/enums/TapSlideBehavior"

// Nombre de générations jouées par étape graphique, en puissance de 2
export const gensPerStep = persisted("gensPerStep", 0)

// Options de rendu
export const trace = persisted("trace", false)
export const grid = persisted("grid", false)

// Paramètres d’affichage de l’interface
export const debuggerMode = persisted("debuggerMode", false)
export const spectatorMode = persisted("spectatorMode", false)

// État de l’affichage
// Décalage en x et y de l’affichage du jeu
export const offset = persisted<Coordinates>("offset", {
	x: innerWidth / 2,
	y: innerHeight / 2,
})
export const zoomLevel = persisted("zoomLevel", DEFAULT_ZOOM_LEVEL - 1)
export const cellSize = derived(zoomLevel, ($zoomLevel) => 2 ** $zoomLevel)

// Comportement de défilement sélectionné
export const scrollBehavior = writable(ScrollBehavior.Zoom)

// Comportement de clic-glissement sélectionné
export const tapSlideBehavior = writable(TapSlideBehavior.Move)

// Mode de modification sélectionné
export const editMode = writable(EditMode.Toggle)

// Stratégie de jeu en cours d’utilisation : cadencé ou effréné
export const playStrategy = persisted("playStrategy", PlayStrategy.Scheduled)

// Jeu en cours de progression
export const isRunning = writable(false)

// Export du modèle pour la sauvegarde
export const patternExport = persisted<string | null>("patternExport", null)

// Fenêtre
export const modal = writable<Modal | null>(null)
