const NAME_LINE = /^! ?Name: (.+)$/
const COMMENT_LINE = /^! ?(.*)$/
// On tolère les fichiers où les points en fin de ligne sont omis, et donc certaines lignes peuvent être vides
const CELLS_LINE = /[O.]*/i

const parseCellsFile = (fileContent: string) => {
	const lines = fileContent.split("\n")

	if (!NAME_LINE.test(lines[0])) {
		throw new SyntaxError("Erreur dans le format du fichier : ligne d’entête manquante")
	}
	const name = lines[0].match(NAME_LINE)![1]
	lines.shift()

	// let commentLinesCount = 0
	const comments: Array<string> = []
	while (COMMENT_LINE.test(lines[0])) {
		comments.push(lines[0].match(COMMENT_LINE)![1])
		lines.shift()
		// commentLinesCount += 1
	}

	const cells = new Set<[number, number]>()
	const startX = -Math.ceil(Math.max(...lines.map((line) => line.replace(/^\.*/, "").replace(/\.*$/, "").length)) / 2)
	const startY = -Math.ceil(lines.filter((line) => line.includes("O")).length / 2)

	let x = startX
	let y = startY
	for (const [index, line] of lines.entries()) {
		if (!CELLS_LINE.test(line)) {
			throw new SyntaxError(`Erreur dans le format du fichier : ligne malformée [ligne ${index + 1}]`)
		}

		for (const character of line) {
			if (character === "O") {
				cells.add([Number(x), Number(y)])
			}
			x += 1
		}

		y += 1
		x = startX
	}

	return { name, cells }
}

export default parseCellsFile
