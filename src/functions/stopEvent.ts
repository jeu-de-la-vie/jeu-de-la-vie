const stopEvent = <T extends Event>(event: T) => {
	event.stopPropagation()
	event.preventDefault()
}

export default stopEvent
