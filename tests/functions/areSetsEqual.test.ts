import { expect, it } from "vitest"

import areSetsEqual from "src/functions/areSetsEqual"

it("compare identic sets", () => {
	const sets = [
		new Set(),
		new Set([0]),
		new Set([1, 2, 3]),
		new Set(["azerty"]),
		new Set([new Set([42])]),
	] as const

	for (const set of sets) {
		expect(areSetsEqual(set, set)).toBe(true)
	}
})

it("compare equal sets", () => {
	const tests = [
		[new Set(), new Set()],
		[new Set([]), new Set([])],
		[new Set([0]), new Set([0])],
		[new Set([1, 2, 3]), new Set([3, 2, 1])],
		[new Set(["azerty", "qwerty"]), new Set(["qwerty", "azerty"])],
	] as const

	for (const [a, b] of tests) {
		expect(areSetsEqual(a, b)).toBe(true)
	}
})

it("compare different sets", () => {
	const tests = [
		[new Set(), new Set([0])],
		[new Set([0]), new Set()],
		[new Set([1, 2, 3]), new Set([1, 2, 4])],
		[new Set(["azerty"]), new Set(["qwerty"])],
		[new Set([new Set([1])]), new Set([new Set([1])])],
	] as const

	for (const [a, b] of tests) {
		expect(areSetsEqual(a, b)).toBe(false)
	}
})
