import type { IconDefinition } from "@fortawesome/free-solid-svg-icons"

import {
	fa1,
	faArrowsToCircle,
	faArrowsToDot,
	faArrowsUpDownLeftRight,
	faBackward,
	faBorderAll,
	faBorderNone,
	faBroom,
	faBug,
	faBugSlash,
	faCircleHalfStroke,
	faCircleInfo,
	faDownload,
	faEraser,
	faEye,
	faEyeSlash,
	faForward,
	faHand,
	faLink,
	faLinkSlash,
	faMagnifyingGlass,
	faMagnifyingGlassMinus,
	faMagnifyingGlassPlus,
	faPause,
	faPencil,
	faPenToSquare,
	faPersonRunning,
	faPersonWalking,
	faPlay,
	faRecycle,
	faStepForward,
	faUpload,
} from "@fortawesome/free-solid-svg-icons"

class KeyboardAction {
	static readonly Pause = new KeyboardAction(faPause)
	static readonly Run = new KeyboardAction(faPlay)
	static readonly PlayStep = new KeyboardAction(faStepForward)
	static readonly OpenFile = new KeyboardAction(faUpload)
	static readonly ExportPattern = new KeyboardAction(faDownload)
	static readonly HideTrace = new KeyboardAction(faLinkSlash)
	static readonly ShowTrace = new KeyboardAction(faLink)
	static readonly HideGrid = new KeyboardAction(faBorderNone)
	static readonly ShowGrid = new KeyboardAction(faBorderAll)
	static readonly EnableDebuggerMode = new KeyboardAction(faBug)
	static readonly DisableDebuggerMode = new KeyboardAction(faBugSlash)
	static readonly EnableSpectatorMode = new KeyboardAction(faEye)
	static readonly DisableSpectatorMode = new KeyboardAction(faEyeSlash)
	static readonly ZoomIn = new KeyboardAction(faMagnifyingGlassPlus)
	static readonly ZoomOut = new KeyboardAction(faMagnifyingGlassMinus)
	static readonly ResetOffset = new KeyboardAction(faArrowsToDot)
	static readonly FitToPattern = new KeyboardAction(faArrowsToCircle)
	static readonly SetScrollBehaviorToMove = new KeyboardAction(faArrowsUpDownLeftRight)
	static readonly SetScrollBehaviorToZoom = new KeyboardAction(faMagnifyingGlass)
	static readonly SetTapSlideBehaviorToMove = new KeyboardAction(faHand)
	static readonly SetTapSlideBehaviorToEdit = new KeyboardAction(faPenToSquare)
	static readonly SetEditModeToCreate = new KeyboardAction(faPencil)
	static readonly SetEditModeToToggle = new KeyboardAction(faCircleHalfStroke)
	static readonly SetEditModeToDelete = new KeyboardAction(faEraser)
	static readonly SetPlayStrategyToScheduled = new KeyboardAction(faPersonWalking)
	static readonly SetPlayStrategyToUnbridled = new KeyboardAction(faPersonRunning)
	static readonly DecreaseSpeed = new KeyboardAction(faBackward)
	static readonly ResetSpeed = new KeyboardAction(fa1)
	static readonly IncreaseSpeed = new KeyboardAction(faForward)
	static readonly ShowHelp = new KeyboardAction(faCircleInfo)
	static readonly WipeSimulation = new KeyboardAction(faBroom)
	static readonly Reset = new KeyboardAction(faRecycle)

	private constructor(public readonly icon: IconDefinition) {}
}

export default KeyboardAction
