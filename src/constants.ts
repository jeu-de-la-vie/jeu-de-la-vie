export const MAXIMUM_SIZE_BYTES = 1_000_000

export const DECIMAL_SIZE_UNITS = ["o", "Ko", "Mo"] as const
export const BINARY_SIZE_UNITS = ["o", "Kio", "Mio"] as const

export const DEFAULT_ZOOM_LEVEL = 4
export const MAXIMUM_ZOOM_LEVEL = 10
export const MINIMUM_ZOOM_LEVEL = -10

export const GRID_MINIMUM_ZOOM_LEVELS = [2, 0, 0]

export const LIGHT_GRID_COLORS = ["#cacaca", "#a2a2a2", "7a7a7a"]
export const LIGHT_CELL_COLOR = "#353535"
export const LIGHT_TRACE_COLORS = [
	"#545454",
	"#6b6b6b",
	"#828282",
	"#999999",
	"#b0b0b0",
	"#c7c7c7",
	"#dedede",
	"#f5f5f5",
]

export const DARK_GRID_COLORS = ["#474747", "#6f6f6f", "#979797"]
export const DARK_CELL_COLOR = "#dfdfdf"
export const DARK_TRACE_COLORS = [
	"#c0c0c0",
	"#a9a9a9",
	"#929292",
	"#7b7b7b",
	"#646464",
	"#4d4d4d",
	"#363636",
	"#1f1f1f",
]

export const RULES_PRESETS = [
	{
		name: "Life",
		birthRules: new Set([3]),
		survivalRules: new Set([2, 3]),
	},
	{
		name: "Highlife",
		birthRules: new Set([3, 6]),
		survivalRules: new Set([2, 3]),
	},
	{
		name: "2x2",
		birthRules: new Set([3, 6]),
		survivalRules: new Set([1, 2, 5]),
	},
	{
		name: "Replicator",
		birthRules: new Set([1, 3, 5, 7]),
		survivalRules: new Set([1, 3, 5, 7]),
	},
]
