import { BINARY_SIZE_UNITS, DECIMAL_SIZE_UNITS } from "src/constants"

import round from "src/functions/round"

interface Options {
	mode?: "binary" | "binary-lower-unit" | "decimal"
	roundPrecision?: number
	locale?: string
	minimumFractionDigits?: number
}

const formatFileSize = (
	sizeBytes: number,
	{
		mode = "binary",
		roundPrecision = 2,
		locale = "fr-FR",
		minimumFractionDigits = 2,
	}: Options,
) => {
	// Détermine la base à considérer pour le calcul de la valeur et les unités à utiliser
	const basePower = mode === "decimal" ? 1_000 : 1_024
	const units = mode === "decimal" ? DECIMAL_SIZE_UNITS : BINARY_SIZE_UNITS

	// Détermine l’unité à considérer pour le formatage
	// Pour les valeurs entre 1_000 et 1_024, formate soit à l’unité actuelle, soit en passant à l’unité supérieure
	const unitIndex = mode === "binary-lower-unit"
		? Math.floor(Math.log2(sizeBytes) / 10)
		: Math.floor(Math.log10(sizeBytes) / 3)

	// Calcule la valeur à afficher dans cette unité
	const value = round(sizeBytes / basePower ** unitIndex, roundPrecision)

	// Détermine le symbole d’unité utilisée
	const unit = units[unitIndex]

	// Et formate la valeur et l’unité
	const formatter = Intl.NumberFormat(locale, { minimumFractionDigits: unitIndex === 0 ? 0 : minimumFractionDigits })
	const formattedValue = formatter.format(value)
	return `${formattedValue} ${unit}`
}

export default formatFileSize
