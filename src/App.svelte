<script lang="ts">
	import { onDestroy, onMount, tick } from "svelte"

	import {
		DARK_CELL_COLOR,
		DARK_GRID_COLORS,
		DARK_TRACE_COLORS,
		DEFAULT_ZOOM_LEVEL,
		GRID_MINIMUM_ZOOM_LEVELS,
		LIGHT_CELL_COLOR,
		LIGHT_GRID_COLORS,
		LIGHT_TRACE_COLORS,
		MAXIMUM_SIZE_BYTES,
		MAXIMUM_ZOOM_LEVEL,
		MINIMUM_ZOOM_LEVEL,
	} from "src/constants"
	import {
		cellSize,
		debuggerMode,
		editMode,
		gensPerStep,
		grid,
		isRunning,
		modal,
		offset,
		patternExport,
		playStrategy,
		scrollBehavior,
		spectatorMode,
		tapSlideBehavior,
		trace,
		zoomLevel,
	} from "src/store"
	import type { Coordinates } from "src/types"

	import EditMode from "src/enums/EditMode"
	import KeyboardAction from "src/enums/KeyboardAction"
	import Modal from "src/enums/Modal"
	import PlayStrategy from "src/enums/PlayStrategy"
	import ScrollBehavior from "src/enums/ScrollBehavior"
	import TapSlideBehavior from "src/enums/TapSlideBehavior"

	import debounce from "src/functions/debounce"
	import formatFileSize from "src/functions/formatFileSize"
	import parseCellsFile from "src/functions/parsers/parseCellsFile"
	import parseLife106File from "src/functions/parsers/parseLife106File"
	import parseRleFile from "src/functions/parsers/parseRleFile"
	import stopEvent from "src/functions/stopEvent"

	import Controls from "src/components/Controls.svelte"
	import DebugHelper from "src/components/DebugHelper.svelte"
	import FileErrorModal from "src/components/FileErrorModal.svelte"
	import FileReceiptReadyModal from "src/components/FileReceiptReadyModal.svelte"
	import HelpModal from "src/components/HelpModal.svelte"
	import KeyboardActionBubble from "src/components/KeyboardActionBubble.svelte"

	import init, { Simulation } from "pkg/jeu_de_la_vie"
	import wasmUrl from "pkg/jeu_de_la_vie_bg.wasm?url"

	interface TapSlideBeginningInfo {
		offset: Coordinates
		cursor: Coordinates
	}

	interface TapSlideLastPositionInfo {
		cell: Coordinates
	}

	// Position du curseur dans les coordonnées de l’écran
	let cursor = $state<Coordinates | null>(null)

	// Coordonnées de la cellule désignée par le curseur
	const cell = $derived.by(() => {
		if (cursor !== null) {
			return {
				x: Math.floor((cursor.x - $offset.x) / $cellSize),
				y: Math.floor((cursor.y - $offset.y) / $cellSize),
			} as Coordinates
		}

		return null
	})

	// Positions du curseur lors des interactions
	let tapSlideBeginningInfo: TapSlideBeginningInfo | null = null
	let tapSlideLastPositionInfo: TapSlideLastPositionInfo | null = null
	let isTapSliding = $state(false)

	// Icône à afficher en bulle
	let keyboardAction = $state<KeyboardAction | null>(null)

	// Animation de jeu
	let runAnimation: number | null = null

	// Simulation et variables miroir
	let simulation: Simulation | null = null
	let generations = $state(0)
	let population = $state(0)
	let birthRules = $state(new Set([3]))
	let survivalRules = $state(new Set([2, 3]))

	// Utilise trois canevas pour gérer séparément et optimiser les rendus graphiques
	// De la trace, de la grille et des cellules vivantes
	let traceCanvas: HTMLCanvasElement
	let traceContext: CanvasRenderingContext2D

	let gridCanvas: HTMLCanvasElement
	let gridContext: CanvasRenderingContext2D

	let cellsCanvas: HTMLCanvasElement
	let cellsContext: CanvasRenderingContext2D

	let fileInput: HTMLInputElement
	let files = $state<FileList | undefined>()
	let fileOutput: HTMLAnchorElement
	let patternName = $state("unnamed pattern")
	let outputFile = $state<string | undefined>()
	let dragCount = 0
	let fileError = $state<string | undefined>()

	const matchResult = matchMedia("(prefers-color-scheme: dark)")
	let darkTheme = $state(matchResult.matches)

	const exportPattern = () => patternExport.set(simulation!.export_rle_format(patternName))

	const resizeCanvas = (canvas: HTMLCanvasElement) => {
		// Dimensionne le canevas pour prendre toute la page
		canvas.width = innerWidth
		canvas.height = innerHeight
	}

	const clearContext = (context: CanvasRenderingContext2D) => context.clearRect(0, 0, innerWidth, innerHeight)
	const clearTrace = () => clearContext(traceContext)
	const clearGrid = () => clearContext(gridContext)
	const clearCells = () => clearContext(cellsContext)

	const drawTrace = ($offset: Coordinates) => {
		clearTrace()

		simulation!.draw_trace(traceContext, $offset.x, $offset.y, $cellSize, darkTheme ? DARK_TRACE_COLORS : LIGHT_TRACE_COLORS)
	}

	const drawStepGrid = ($offset: Coordinates, step: number, color: string) => {
		gridContext.beginPath()
		// Prépare le tracé de la grille
		gridContext.strokeStyle = color
		gridContext.lineWidth = 2 ** ($zoomLevel - DEFAULT_ZOOM_LEVEL)

		const gridStep = $cellSize * step

		// Affiche les tracés en colonne
		// Cale le déplacement en x dans la largeur de l’écran puis prépare le décalage initial à gauche
		let x = $offset.x % gridStep

		// Et affiche une colonne à intervalles réguliers sur toute la largeur de l’écran
		while (x < innerWidth) {
			gridContext.moveTo(x, 0)
			gridContext.lineTo(x, innerHeight)

			x += gridStep
		}

		// Affiche les tracés en ligne
		// Cale le déplacement en y dans la hauteur de l’écran puis prépare le décalage initial en haut
		let y = $offset.y % gridStep

		// Et affiche une ligne à intervalles réguliers sur toute la hauteur de l’écran
		while (y < innerHeight) {
			gridContext.moveTo(0, y)
			gridContext.lineTo(innerWidth, y)

			y += gridStep
		}

		gridContext.stroke()
		gridContext.closePath()
	}

	const drawGrid = ($offset: Coordinates) => {
		clearGrid()

		// Trace des grilles correspondant à des tailles différentes
		// Pour faciliter sa lecture à de grandes échelles
		for (const [i, stepSize] of [1, 10, 100].entries()) {
			if ($zoomLevel >= GRID_MINIMUM_ZOOM_LEVELS[i]) {
				const color = darkTheme ? DARK_GRID_COLORS[i] : LIGHT_GRID_COLORS[i]
				drawStepGrid($offset, stepSize, color)
			}
		}
	}

	const drawCells = ($offset: Coordinates) => {
		clearCells()

		simulation!.draw_cells(cellsContext, $offset.x, $offset.y, $cellSize, darkTheme ? DARK_CELL_COLOR : LIGHT_CELL_COLOR)
	}

	const drawSimulation = ($offset: Coordinates) => {
		if ($trace) {
			drawTrace($offset)
		}
		if ($grid) {
			drawGrid($offset)
		}
		drawCells($offset)
	}

	const resizeCanvasAndDrawSimulation = () => {
		// Redimensionne les canevas
		resizeCanvas(traceCanvas)
		resizeCanvas(gridCanvas)
		resizeCanvas(cellsCanvas)

		// Puis met à jour l’affichage
		drawSimulation($offset)
	}

	const playStep = () => {
		for (let i = 0; i < 2 ** $gensPerStep; i += 1) {
			simulation!.calculate_generation()
		}

		generations = simulation!.generations
		population = simulation!.population
		if ($trace) {
			drawTrace($offset)
		}
		drawCells($offset)
	}

	const playStepAndExport = () => {
		playStep()
		exportPattern()
	}

	const runScheduled = () => {
		playStep()
		runAnimation = requestAnimationFrame(runScheduled)
	}

	const runUnbridled = () => {
		playStep()
		runAnimation = setTimeout(runUnbridled, 0)
	}

	matchResult.addEventListener("change", ({ matches }) => {
		darkTheme = matches
		drawSimulation($offset)
	})

	const onmousedown = (event: MouseEvent) => {
		// Ne réagit qu’au bouton principal d’un pointeur interactif
		if (event.button === 0) {
			tapSlideBeginningInfo = {
				offset: {
					x: $offset.x,
					y: $offset.y,
				},
				cursor: {
					x: event.clientX,
					y: event.clientY,
				},
			}

			tapSlideLastPositionInfo = {
				cell: {
					x: event.clientX,
					y: event.clientY,
				},
			}
		}
	}

	const onTapSlideMove = (event: MouseEvent) => {
		if (tapSlideBeginningInfo !== null) {
			if ($tapSlideBehavior === TapSlideBehavior.Move) {
				// On ne définit isTapSliding qu’à partir du moment où on a commencé à déplacer le curseur (au contraire d’un simple clic)
				isTapSliding = true
				offset.set({
					x: tapSlideBeginningInfo!.offset.x + (event.clientX - tapSlideBeginningInfo!.cursor.x),
					y: tapSlideBeginningInfo!.offset.y + (event.clientY - tapSlideBeginningInfo!.cursor.y),
				})
			}

			else if ($tapSlideBehavior === TapSlideBehavior.Edit) {
				if (cell !== null && (cell.x !== tapSlideLastPositionInfo!.cell.x || cell.y !== tapSlideLastPositionInfo!.cell.y)) {
					isTapSliding = true
					tapSlideLastPositionInfo = {
						cell: {
							x: cell.x,
							y: cell.y,
						},
					}

					if ($editMode === EditMode.Create) {
						simulation!.create_at(cell.x, cell.y)
					}

					else if ($editMode === EditMode.Toggle) {
						simulation!.toggle_at(cell.x, cell.y)
					}

					else if ($editMode === EditMode.Delete) {
						simulation!.delete_at(cell.x, cell.y)
					}

					population = simulation!.population
					drawCells($offset)
				}
			}
		}
	}

	const onTapSlideEnd = () => {
		isTapSliding = false
		tapSlideBeginningInfo = null
		tapSlideLastPositionInfo = null

		if (!$isRunning && $tapSlideBehavior === TapSlideBehavior.Edit) {
			exportPattern()
		}
	}

	onMount(async () => {
		traceContext = traceCanvas.getContext("2d")!
		gridContext = gridCanvas.getContext("2d")!
		cellsContext = cellsCanvas.getContext("2d")!

		// En cas de redimensionnement de la page
		window.addEventListener("resize", resizeCanvasAndDrawSimulation)

		// Gestion du déplacement de la carte et de la manipulation des cellules (le curseur doit pouvoir sortir de la fenêtre sans terminer l’action)
		window.addEventListener("mousemove", onTapSlideMove)
		window.addEventListener("mouseup", onTapSlideEnd)

		await init(wasmUrl)
		simulation = new Simulation()

		// Puis charge le dernier état sauvegardé
		if ($patternExport !== null) {
			// Et restaure les règles de naissance et de survie, puis ajoute les cellules vivantes
			const { cells: cellules, birthRules: newBirthRules, survivalRules: newSurvivalRules } = parseRleFile($patternExport)

			if (newBirthRules !== undefined && newSurvivalRules !== undefined) {
				simulation.empty_birth_rule()
				simulation.empty_survival_rule()

				for (const rule of newBirthRules) {
					simulation.add_birth_rule(rule)
				}

				for (const rule of newSurvivalRules) {
					simulation.add_survival_rule(rule)
				}
			}

			for (const [x, y] of cellules) {
				simulation.toggle_at(x, y)
			}

			population = simulation.population
		}

		trace.subscribe(($trace) => $trace ? drawTrace($offset) : clearTrace())

		grid.subscribe(($grid) => $grid ? drawGrid($offset) : clearGrid())

		isRunning.subscribe(($isRunning) => {
			if ($isRunning) {
				runAnimation = $playStrategy === PlayStrategy.Scheduled
					? requestAnimationFrame(runScheduled)
					: setTimeout(runUnbridled, 0)
			}
			else {
				$playStrategy === PlayStrategy.Scheduled
					? cancelAnimationFrame(runAnimation as number)
					: clearTimeout(runAnimation as number)

				runAnimation = null
				exportPattern()
			}
		})

		offset.subscribe(($offset) => drawSimulation($offset))

		// Et au premier chargement
		resizeCanvasAndDrawSimulation()
	})

	onDestroy(() => {
		window.removeEventListener("resize", resizeCanvasAndDrawSimulation)
		window.removeEventListener("mousemove", onTapSlideMove)
		window.removeEventListener("mouseup", onTapSlideEnd)
	})

	// Fonctions

	const zoomIn = () => {
		if ($zoomLevel <= MAXIMUM_ZOOM_LEVEL - 1) {
			zoomLevel.update(($zoomLevel) => $zoomLevel + 1)
			offset.update(($offset) => ({
				x: $offset.x * 2 - innerWidth / 2,
				y: $offset.y * 2 - innerHeight / 2,
			}))
		}
	}

	const zoomOut = () => {
		if ($zoomLevel >= MINIMUM_ZOOM_LEVEL + 1) {
			zoomLevel.update(($zoomLevel) => $zoomLevel - 1)
			offset.update(($offset) => ({
				x: $offset.x / 2 + innerWidth / 4,
				y: $offset.y / 2 + innerHeight / 4,
			}))
		}
	}

	const resetOffset = () => {
		offset.set({
			x: innerWidth / 2,
			y: innerHeight / 2,
		})
	}

	const resetZoomLevel = () => {
		zoomLevel.set(DEFAULT_ZOOM_LEVEL)
	}

	const fitToPattern = () => {
		if (population === 0) {
			resetZoomLevel()
			offset.set({
				x: innerWidth / 2,
				y: innerHeight / 2,
			})
			return
		}

		const { min_x, min_y, max_x, max_y } = simulation!.get_bounds()
		const patternWidth = max_x - min_x + 1
		const patternHeight = max_y - min_y + 1

		// Calcule le niveau de grossissement nécessaire pour afficher toutes les cellules
		// En laissant 5 % de marge sur les bords en largeur et en hauteur
		const newZoomLevel = Math.min(
			Math.log2(innerWidth * 0.9 / patternWidth),
			Math.log2(innerHeight * 0.9 / patternHeight),
		)
		const newCellSize = 2 ** newZoomLevel

		zoomLevel.set(newZoomLevel)
		offset.set({
			x: innerWidth / 2 - (min_x + patternWidth / 2) * newCellSize,
			y: innerHeight / 2 - (min_y + patternHeight / 2) * newCellSize,
		})

		drawSimulation($offset)
	}

	const showFileErrorModal = () => modal.set(Modal.FileError)
	const showHelpModal = () => modal.set(Modal.ApplicationHelp)

	// Masque la bulle après un délai donné, avec un système de rafraichissement du délai
	const hideBubble = debounce(() => {
		keyboardAction = null
	}, 1_000) as () => void

	// Affiche puis masque la bulle
	const showBubble = (newKeyboardAction: KeyboardAction) => {
		// Réinitialise d’abord la variable
		keyboardAction = null

		// Puis la redéfinit pour garantir qu’elle est rafraichie, après un certain délai garantissant que les deux mises à jour se font indépendamment
		setTimeout(() => {
			keyboardAction = newKeyboardAction
			hideBubble()
		}, 100)
	}

	const useScheduledStrategy = () => {
		const shouldKeepRunning = $isRunning
		isRunning.set(false)

		playStrategy.set(PlayStrategy.Scheduled)

		isRunning.set(shouldKeepRunning)
	}

	const useUnbridledStrategy = () => {
		const shouldKeepRunning = $isRunning
		isRunning.set(false)

		playStrategy.set(PlayStrategy.Unbridled)

		isRunning.set(shouldKeepRunning)
	}

	const onwheel = (event: WheelEvent) => {
		event.stopPropagation()

		if ($scrollBehavior === ScrollBehavior.Zoom) {
			const zoomDelta = event.deltaY / 512
			const newZoomLevel = $zoomLevel - zoomDelta
			// Alternative délaissée pour minimiser le cout de calcul
			// const newZoomLevel = Math.min(MAXIMUM_ZOOM_LEVEL, Math.max(MINIMUM_ZOOM_LEVEL, $zoomLevel - zoomDelta))
			// const clampedZoomDelta = $zoomLevel - newZoomLevel

			if (newZoomLevel > MINIMUM_ZOOM_LEVEL && newZoomLevel < MAXIMUM_ZOOM_LEVEL) {
				zoomLevel.set(newZoomLevel)
				offset.update(($offset) => ({
					x: event.clientX - (event.clientX - $offset.x) / 2 ** zoomDelta,
					y: event.clientY - (event.clientY - $offset.y) / 2 ** zoomDelta,
				}))
			}
		}

		else if ($scrollBehavior === ScrollBehavior.Move) {
			offset.update(($offset) => ({
				x: $offset.x - Math.round(event.deltaX),
				y: $offset.y - Math.round(event.deltaY),
			}))
		}
	}

	const onmouseup = (event: MouseEvent) => {
		// L’évènement click est déclenché après un mousedown suivi d’un mouseup
		// On ne peut donc pas compter sur l’existence de isTapSliding à ce moment‐là puisqu’il est détruit dans le gestionnaire du mouseup sur Window
		// On doit donc intégrer la gestion du clic dans un gestionnaire de mouseup sur la <div> concernée, qui sera déclenché avant d’atteindre Window
		// De même que dans le gestionnaire de mousedown, on ne réagit qu’au bouton principal du pointeur interactif
		if (event.button === 0 && $tapSlideBehavior === TapSlideBehavior.Edit && !isTapSliding) {
			if (cell !== null) {
				if ($editMode === EditMode.Create) {
					simulation!.create_at(cell.x, cell.y)
				}

				else if ($editMode === EditMode.Toggle) {
					simulation!.toggle_at(cell.x, cell.y)
				}

				else if ($editMode === EditMode.Delete) {
					simulation!.delete_at(cell.x, cell.y)
				}

				// Et ne prend la peine de sauvegarder et dessiner que si le jeu était en pause
				if (!$isRunning) {
					exportPattern()
					drawCells($offset)
				}
			}
		}
	}

	const onmousemove = (event: MouseEvent) => {
		cursor = {
			x: event.clientX,
			y: event.clientY,
		}
	}

	const onmouseleave = () => {
		cursor = null
	}

	const decreaseSpeed = () => {
		gensPerStep.update(($gensPerStep) => $gensPerStep === 0 ? 0 : $gensPerStep - 1)
	}

	const resetSpeed = () => {
		gensPerStep.set(0)
	}

	const increaseSpeed = () => {
		gensPerStep.update(($gensPerStep) => $gensPerStep + 1)
	}

	const openFile = () => fileInput?.click()

	const loadFile = (fileList: FileList) => {
		fileError = undefined

		// S’il y a plusieurs fichiers, ou aucun, rejette le dépôt
		if (fileList.length !== 1) {
			fileError = fileList.length === 0
				? "Aucun fichier reçu. Veuillez déposer un fichier."
				: "Trop de fichiers reçus. Veuillez ne déposer qu’un seul fichier."
			showFileErrorModal()
			return
		}

		const file = fileList[0]
		// Si le poids du fichier fourni dépasse la taille maximale attendue
		if (file.size > MAXIMUM_SIZE_BYTES) {
			const maximumSize = formatFileSize(MAXIMUM_SIZE_BYTES, { roundPrecision: 0 })
			fileError = `Fichier trop volumineux. Veuillez déposer un fichier de poids inférieur à ${maximumSize}.`
			showFileErrorModal()
			return
		}

		const fileReader = new FileReader()

		// Charge son contenu
		fileReader.addEventListener("load", () => {
			const fileContent = (fileReader.result as string)
				.replace(/\r\n/g, "\n")
				.replace(/\r/g, "\n")
			const fileExtension = file.name.split(".").pop()!
			let name: string | undefined
			let cells: Set<[number, number]>
			let newBirthRules: Set<number> | undefined
			let newSurvivalRules: Set<number> | undefined

			try {
				if (fileExtension === "cells") {
					({ name, cells } = parseCellsFile(fileContent))
				}

				else if (["lif", "life"].includes(fileExtension)) {
					cells = parseLife106File(fileContent)
				}

				else if (fileExtension === "rle") {
					({ name, cells, birthRules: newBirthRules, survivalRules: newSurvivalRules } = parseRleFile(fileContent))
				}

				else {
					fileError = "Le format de ce fichier n’est pas connu. Veuillez déposer un fichier au format Cells, Life 1.06 ou RLE avec l’extension de fichier appropriée."
					showFileErrorModal()
					return
				}
			}
			catch (exception) {
				fileError = (exception as SyntaxError).message
				showFileErrorModal()
				return
			}

			isRunning.set(false)
			simulation!.reset()
			if (newBirthRules !== undefined && newSurvivalRules !== undefined) {
				simulation!.empty_birth_rule()
				simulation!.empty_survival_rule()

				for (const rule of newBirthRules) {
					simulation!.add_birth_rule(rule)
				}

				for (const rule of newSurvivalRules) {
					simulation!.add_survival_rule(rule)
				}

				birthRules = newBirthRules
				survivalRules = newSurvivalRules
			}
			patternName = name ?? "unnamed pattern"

			for (const [x, y] of cells) {
				simulation!.create_at(x, y)
			}
			generations = simulation!.generations
			population = simulation!.population

			resetSpeed()
			fitToPattern()
			drawSimulation($offset)
		})

		fileReader.readAsText(file)
	}

	$effect(() => {
		if (files) {
			loadFile(files)
		}
	})

	const downloadPattern = async () => {
		// Déclenche la sauvegarde du fichier
		outputFile = `data:text/plain;base64,${btoa($patternExport!)}`
		await tick()
		fileOutput.click()
	}

	const toggleBirthRule = (i: number) => {
		simulation!.toggle_birth_rule(i)

		const result = new Set(birthRules)
		result.has(i) ? result.delete(i) : result.add(i)
		birthRules = result
	}

	const toggleSurvivalRule = (i: number) => {
		simulation!.toggle_survival_rule(i)

		const result = new Set(survivalRules)
		result.has(i) ? result.delete(i) : result.add(i)
		survivalRules = result
	}

	const usePresetRule = (birth: Set<number>, survival: Set<number>) => {
		simulation!.empty_birth_rule()
		simulation!.empty_survival_rule()

		for (const rule of birth) {
			simulation!.add_birth_rule(rule)
		}

		for (const rule of survival) {
			simulation!.add_survival_rule(rule)
		}

		birthRules = new Set(birth)
		survivalRules = new Set(survival)
	}

	const wipeSimulation = () => {
		isRunning.set(false)
		simulation!.wipe()
		generations = simulation!.generations
		population = simulation!.population
		clearTrace()
		clearCells()
		exportPattern()
	}

	const reset = () => {
		isRunning.set(false)
		simulation!.reset()
		patternName = "unnamed pattern"
		generations = simulation!.generations
		population = simulation!.population
		birthRules = new Set([3])
		survivalRules = new Set([2, 3])
		trace.set(false)
		clearTrace()
		grid.set(false)
		clearGrid()
		clearCells()
		debuggerMode.set(false)
		spectatorMode.set(false)
		resetOffset()
		resetZoomLevel()
		resetSpeed()
		useScheduledStrategy()
		exportPattern()
	}

	const ondragenter = (event: DragEvent) => {
		stopEvent(event)

		// Le compteur est nécessaire puisque le gestionnaire d’évènements redéclenche un évènement d’entrée ou de sortie pour tout élément pour lequel le glisser a lieu, soit le canevas ou un panneau en premier lieu, puis la fenêtre quand celle-ci se superpose
		// Il est donc nécessaire de fermer la fenêtre uniquement quand le premier élément survolé reçoit l’évènement de sortie, et non un de ses enfants
		// La seule solution est de compter les entrées et sorties pour déterminer précisément si tous les éléments entrés ont reçu la sortie associée
		dragCount += 1
	}

	const ondragover = (event: DragEvent) => {
		stopEvent(event)

		modal.set(Modal.FileReceiptReady)
	}

	const ondragleave = (event: DragEvent) => {
		stopEvent(event)

		dragCount -= 1
		if (dragCount === 0) {
			modal.set(null)
		}
	}

	const ondrop = (event: DragEvent) => {
		stopEvent(event)

		modal.set(null)
		dragCount = 0

		// Traite les fichiers reçus
		loadFile(event.dataTransfer!.files)
	}

	const onkeydown = (event: KeyboardEvent) => {
		if (
			event.altKey
			|| event.ctrlKey
			|| event.metaKey
			|| event.shiftKey
			|| (event.target instanceof HTMLInputElement && event.target.type === "text")
		) {
			return
		}

		// Jouer et mettre en pause
		if (
			event.key === " "
			&& !(
				event.target instanceof HTMLButtonElement
				|| (event.target as HTMLElement).getAttribute("role") === "button"
			)
		) {
			isRunning.update(($isRunning) => !$isRunning)
			showBubble($isRunning ? KeyboardAction.Run : KeyboardAction.Pause)
		}

		// Jouer une étape
		else if (event.key === "e" && !$isRunning) {
			playStepAndExport()
			showBubble(KeyboardAction.PlayStep)
		}

		// Déplacement de la carte
		else if (event.key === "ArrowLeft") {
			offset.update(($offset) => ({
				x: $offset.x + 2 ** DEFAULT_ZOOM_LEVEL,
				y: $offset.y,
			}))
		}

		else if (event.key === "ArrowRight") {
			offset.update(($offset) => ({
				x: $offset.x - 2 ** DEFAULT_ZOOM_LEVEL,
				y: $offset.y,
			}))
		}

		else if (event.key === "ArrowUp") {
			offset.update(($offset) => ({
				x: $offset.x,
				y: $offset.y + 2 ** DEFAULT_ZOOM_LEVEL,
			}))
		}

		else if (event.key === "ArrowDown") {
			offset.update(($offset) => ({
				x: $offset.x,
				y: $offset.y - 2 ** DEFAULT_ZOOM_LEVEL,
			}))
		}

		else if (event.key === "j") {
			if ($scrollBehavior === ScrollBehavior.Zoom) {
				scrollBehavior.set(ScrollBehavior.Move)
				showBubble(KeyboardAction.SetScrollBehaviorToMove)
			}
			else {
				scrollBehavior.set(ScrollBehavior.Zoom)
				showBubble(KeyboardAction.SetScrollBehaviorToZoom)
			}
		}

		else if (event.key === "m") {
			if ($tapSlideBehavior === TapSlideBehavior.Move) {
				tapSlideBehavior.set(TapSlideBehavior.Edit)
				showBubble(KeyboardAction.SetTapSlideBehaviorToEdit)
			}
			else {
				tapSlideBehavior.set(TapSlideBehavior.Move)
				showBubble(KeyboardAction.SetTapSlideBehaviorToMove)
			}
		}

		else if (event.key === "l" && $tapSlideBehavior === TapSlideBehavior.Edit) {
			if ($editMode === EditMode.Create) {
				editMode.set(EditMode.Toggle)
				showBubble(KeyboardAction.SetEditModeToToggle)
			}
			else if ($editMode === EditMode.Toggle) {
				editMode.set(EditMode.Delete)
				showBubble(KeyboardAction.SetEditModeToDelete)
			}
			else {
				editMode.set(EditMode.Create)
				showBubble(KeyboardAction.SetEditModeToCreate)
			}
		}

		else if (event.key === "x") {
			if ($playStrategy === PlayStrategy.Scheduled) {
				useUnbridledStrategy()
				showBubble(KeyboardAction.SetPlayStrategyToUnbridled)
			}
			else {
				useScheduledStrategy()
				showBubble(KeyboardAction.SetPlayStrategyToScheduled)
			}
		}

		else if (event.key === "PageUp") {
			increaseSpeed()
			showBubble(KeyboardAction.IncreaseSpeed)
		}

		else if (event.key === "PageDown") {
			decreaseSpeed()
			showBubble(KeyboardAction.DecreaseSpeed)
		}

		else if (event.key === "v") {
			resetSpeed()
			showBubble(KeyboardAction.ResetSpeed)
		}

		else if (event.key === "Home") {
			zoomIn()
			showBubble(KeyboardAction.ZoomIn)
		}

		else if (event.key === "End") {
			zoomOut()
			showBubble(KeyboardAction.ZoomOut)
		}

		else if (event.key === "c") {
			resetOffset()
			showBubble(KeyboardAction.ResetOffset)
		}

		else if (event.key === "f") {
			fitToPattern()
			showBubble(KeyboardAction.FitToPattern)
		}

		// Options

		else if (event.key === "h") {
			showHelpModal()
		}

		// Ouvrir un fichier de modèle à charger
		else if (event.key === "o") {
			openFile()
			showBubble(KeyboardAction.OpenFile)
		}

		// Télécharger le modèle
		else if (event.key === "p") {
			downloadPattern()
			showBubble(KeyboardAction.ExportPattern)
		}

		else if (event.key === "t") {
			trace.update(($trace) => !$trace)
			showBubble($trace ? KeyboardAction.ShowTrace : KeyboardAction.HideTrace)
		}

		else if (event.key === "g") {
			grid.update(($grid) => !$grid)
			showBubble($grid ? KeyboardAction.ShowGrid : KeyboardAction.HideGrid)
		}

		else if (event.key === "d") {
			debuggerMode.update(($debuggerMode) => !$debuggerMode)
			showBubble($debuggerMode ? KeyboardAction.EnableDebuggerMode : KeyboardAction.DisableDebuggerMode)
		}

		else if (event.key === "s") {
			spectatorMode.update(($spectatorMode) => !$spectatorMode)
			showBubble($spectatorMode ? KeyboardAction.EnableSpectatorMode : KeyboardAction.DisableSpectatorMode)
		}

		else if (event.key === "w") {
			wipeSimulation()
			showBubble(KeyboardAction.WipeSimulation)
		}

		// Réinitialiser l’état de l’application
		else if (event.key === "Backspace") {
			reset()
			showBubble(KeyboardAction.Reset)
		}
	}
</script>

<svelte:head>
	<link rel="icon" type="image/svg" href="/icons/icon.svg" />
	<link rel="icon" type="image/png" href="/icons/{darkTheme ? "dark" : "light"}.png" />
</svelte:head>

<svelte:body
	{ondragenter}
	{ondragover}
	{ondragleave}
	{ondrop}
	{onkeydown}
/>

<!-- Import et export de fichier -->
<input
	type="file"
	bind:this={fileInput}
	style:display="none"
	bind:files={files}
	aria-hidden="true"
/>
<a
	href={outputFile}
	download="{patternName}.rle"
	bind:this={fileOutput}
	style:display="none"
	aria-hidden="true"
>
	Sauvegarder…
</a>

<div class="app">
	<div
		class={$tapSlideBehavior === TapSlideBehavior.Move ? (isTapSliding ? "cursor-grabbing" : "cursor-grab") : "cursor-pointer"}
		{onmousedown}
		{onmousemove}
		{onmouseup}
		{onmouseleave}
		{onwheel}
		role="presentation"
	>
		<!-- Canevas de rendu de la simulation -->
		<canvas class="absolute" bind:this={traceCanvas}></canvas>
		<canvas class="absolute" bind:this={cellsCanvas}></canvas>
		<canvas class="absolute" bind:this={gridCanvas}></canvas>

		{#if $debuggerMode}
			<DebugHelper {cursor} />
		{/if}
	</div>

	<Controls
		bind:patternName={patternName}

		playStep={playStepAndExport}

		speed={$gensPerStep}
		{decreaseSpeed}
		{resetSpeed}
		{increaseSpeed}

		{openFile}
		{downloadPattern}

		{wipeSimulation}
		{reset}

		{showHelpModal}

		{birthRules}
		{survivalRules}
		{toggleBirthRule}
		{toggleSurvivalRule}
		{usePresetRule}

		{isTapSliding}

		{zoomIn}
		{zoomOut}
		{resetOffset}
		{fitToPattern}

		{generations}
		{population}
		{cell}
		{cursor}
	/>
</div>

{#if keyboardAction !== null}
	<KeyboardActionBubble {keyboardAction} />
{/if}

{#if $modal === Modal.ApplicationHelp}
	<HelpModal />
{/if}
{#if $modal === Modal.FileReceiptReady}
	<FileReceiptReadyModal />
{/if}
{#if $modal === Modal.FileError && fileError !== undefined}
	<FileErrorModal message={fileError} />
{/if}

<style lang="postcss">
	.app {
		@apply h-screen text-neutral-900 bg-neutral-50;
		@apply dark:text-neutral-50 dark:bg-neutral-900;
		font-size: 1em;
		font-weight: 400;
		font-family: BlinkMacSystemFont, -apple-system, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
	}
</style>
