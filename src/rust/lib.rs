// Aide : https://rustwasm.github.io/docs/wasm-bindgen/
use rustc_hash::{FxHashMap, FxHashSet};

use wasm_bindgen::prelude::*;
use web_sys::CanvasRenderingContext2d;

const RLE_FILE_LINE_LENGTH: usize = 70;

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
struct Cell {
	x: i32,
	y: i32,
}

impl Cell {
	const fn neighbours(self) -> [Self; 8] {
		[
			Self { x: self.x - 1, y: self.y - 1 },
			Self { x: self.x - 1, y: self.y },
			Self { x: self.x - 1, y: self.y + 1 },
			Self { x: self.x,     y: self.y - 1 },
			Self { x: self.x,     y: self.y + 1 },
			Self { x: self.x + 1, y: self.y - 1 },
			Self { x: self.x + 1, y: self.y },
			Self { x: self.x + 1, y: self.y + 1 },
		]
	}
}

#[wasm_bindgen]
#[derive(Clone, Copy, Default)]
pub struct Rule {
	values: u16,
}

#[wasm_bindgen]
impl Rule {
	fn new(values: &[u8]) -> Self {
		Self {
			values: values.iter().map(|i| 1 << i).sum(),
		}
	}

	#[allow(clippy::missing_const_for_fn)]
	#[must_use]
	pub fn contains(&self, i: u8) -> bool {
		self.values & (1 << i) != 0
	}

	pub fn add(&mut self, i: u8) {
		self.values |= 1 << i;
	}

	pub fn remove(&mut self, i: u8) {
		self.values &= ! (1 << i);
	}

	pub fn toggle(&mut self, i: u8) {
		if self.contains(i) {
			self.remove(i);
		}
		else {
			self.add(i);
		}
	}

	pub fn empty(&mut self) {
		self.values = 0;
	}
}

#[wasm_bindgen]
pub struct Bounds {
	pub min_x: i32,
	pub min_y: i32,
	pub max_x: i32,
	pub max_y: i32,
}

#[wasm_bindgen]
#[derive(Default)]
pub struct Simulation {
	// Ensemble des cellules actives
	cells: FxHashSet<Cell>,
	// Mémoire des cellules mortes durant les huit générations précédentes
	previous_dead_cells: [Option<FxHashSet<Cell>>; 8],
	// Nombre de cellules vivantes dans le voisinage d’une cellule
	neighbourhoods: FxHashMap<Cell, u8>,
	// Règles de naissance et de survie des cellules
	pub birth: Rule,
	pub survival: Rule,
	// Nombre de générations calculées et population actuelle
	pub generations: u32,
	pub population: u32,
}

#[wasm_bindgen]
impl Simulation {
	#[wasm_bindgen(constructor)]
	#[must_use]
	pub fn new() -> Self {
		Self {
			cells: FxHashSet::default(),
			previous_dead_cells: [None, None, None, None, None, None, None, None],
			neighbourhoods: FxHashMap::default(),
			birth: Rule::new(&[3]),
			survival: Rule::new(&[2, 3]),
			generations: 0,
			population: 0,
		}
	}

	fn create(&mut self, cell: Cell) {
		// Met à jour le décompte des cellules voisines actives de chacune de ses voisines
		for neighbour in cell.neighbours() {
			let neighbours_count = *self.neighbourhoods.get(&neighbour).unwrap_or(&0);

			// En l’incrémentant s’il est déjà défini ou en l’initialisant à 1 autrement
			self.neighbourhoods.insert(neighbour, neighbours_count + 1);
		}

		// Et ajoute finalement la cellule
		self.cells.insert(cell);
		self.population += 1;
	}

	fn delete(&mut self, cellule: Cell) {
		// Met à jour le décompte des cellules voisines actives de chacune de ses voisines
		for neighbour in cellule.neighbours() {
			let neighbours_count = self.neighbourhoods.get(&neighbour).unwrap() - 1;

			// En le décrémentant s’il reste positif
			if neighbours_count > 0 {
				self.neighbourhoods.insert(neighbour, neighbours_count);
			}

			// Ou en le supprimant autrement
			else {
				self.neighbourhoods.remove(&neighbour);
			}
		}

		// Et supprime finalement la cellule
		self.cells.remove(&cellule);
		self.population -= 1;
	}

	pub fn calculate_generation(&mut self) {
		// Initialise les listes des cellules à ajouter et à supprimer à la fin de cette itération
		// Les modifications seront faites sur place après calcul pour optimiser la consommation de mémoire et donc le temps d’exécution
		let mut created_cells = FxHashSet::default();
		let mut deleted_cells = FxHashSet::default();

		// Pour chaque cellule actuellement active
		for cell in &self.cells {
			let neighbours_count = self.neighbourhoods.get(cell).unwrap_or(&0);

			// La marque à supprimer si elle doit mourir
			if ! self.survival.contains(*neighbours_count) {
				deleted_cells.insert(*cell);
			}
		}

		// Pour chaque cellule ayant des voisines enregistrées
		for (cell, neighbours_count) in &self.neighbourhoods {
			// La marque à ajouter si elle doit naitre
			if self.birth.contains(*neighbours_count) && ! self.cells.contains(cell) {
				created_cells.insert(*cell);
			}
		}

		// Retire les cellules à retirer
		for cell in &deleted_cells {
			self.delete(*cell);
		}

		// Et ajoute les cellules à ajouter
		for cell in created_cells {
			self.create(cell);
		}

		self.generations += 1;
		// Ajoute la liste des cellules supprimées à la liste
		self.previous_dead_cells.rotate_right(1);
		self.previous_dead_cells[0] = Some(deleted_cells);
	}

	fn contains(&self, cell: Cell) -> bool {
		self.cells.contains(&cell)
	}

	#[must_use]
	pub fn contains_at(&self, x: i32, y: i32) -> bool {
		let cell = Cell { x, y };
		self.contains(cell)
	}

	pub fn create_at(&mut self, x: i32, y: i32) {
		let cell = Cell { x, y };

		if ! self.contains(cell) {
			self.create(cell);
		}
	}

	pub fn delete_at(&mut self, x: i32, y: i32) {
		let cell = Cell { x, y };

		if self.contains(cell) {
			self.delete(cell);
		}
	}

	pub fn toggle_at(&mut self, x: i32, y: i32) {
		let cell = Cell { x, y };

		if self.contains(cell) {
			self.delete(cell);
		}
		else {
			self.create(cell);
		}
	}

	// Autorise le passage par valeur car Vec<String> ne satisfait pas le trait RefFromWasmAbi pour le passage par pointeur
	#[allow(clippy::needless_pass_by_value)]
	pub fn draw_trace(
		&self,
		canvas_context: &CanvasRenderingContext2d,
		x_offset: f64,
		y_offset: f64,
		cell_size: f64,
		trace_colors: Vec<String>,
	) {
		// Dessine toutes les cellules mortes récemment
		for (i, option_cells) in self.previous_dead_cells.iter().enumerate().rev() {
			if let Some(cells) = option_cells {
				// Ajuste la couleur de la trace en fonction de son ancienneté
				canvas_context.set_fill_style_str(trace_colors[i].as_str());

				for Cell { x, y } in cells {
					canvas_context.fill_rect(
						f64::from(*x).mul_add(cell_size, x_offset),
						f64::from(*y).mul_add(cell_size, y_offset),
						cell_size,
						cell_size,
					);
				}
			}
		}
	}

	pub fn draw_cells(
		&self,
		canvas_context: &CanvasRenderingContext2d,
		x_offset: f64,
		y_offset: f64,
		cell_size: f64,
		color: &str,
	) {
		// Prépare la couleur des cellules
		canvas_context.set_fill_style_str(color);

		// Dessine toutes les cellules actives
		for Cell { x, y } in &self.cells {
			canvas_context.fill_rect(
				f64::from(*x).mul_add(cell_size, x_offset),
				f64::from(*y).mul_add(cell_size, y_offset),
				cell_size,
				cell_size,
			);
		}
	}

	#[must_use]
	pub fn export_rle_format(&self, pattern_name: &str) -> String {
		let mut start_x = i32::MAX;
		let mut start_y = i32::MAX;
		let mut end_x = i32::MIN;
		let mut end_y = i32::MIN;

		// Identifie les coordonnées du rectangle minimal recouvrant toutes les cellules actives
		for Cell{x, y} in &self.cells {
			start_x = start_x.min(*x);
			start_y = start_y.min(*y);
			end_x = end_x.max(*x);
			end_y = end_y.max(*y);
		}

		let mut output_pattern = format!("#N {pattern_name}\n");

		let width = end_x - (start_x - 1);
		let height = end_y - (start_y - 1);

		let birth_rules = (0..10).map(|i| if self.birth.contains(i) { format!("{i}") } else { String::new() }).collect::<String>();
		let survival_rules = (0..10).map(|i| if self.survival.contains(i) { format!("{i}") } else { String::new() }).collect::<String>();

		// Si le plan est vide, retourne un fichier indiquant des dimensions nulles et ne contenant aucune ligne de cellule
		if width == i32::MIN && height == i32::MIN {
			output_pattern.push_str(&format!("x = 0, y = 0, rule = b{birth_rules}/s{survival_rules}\n"));
			return output_pattern;
		}

		output_pattern.push_str(&format!("x = {width}, y = {height}, rule = b{birth_rules}/s{survival_rules}\n"));

		let mut cells_grid = vec![];
		for y in start_y..(start_y + height) {
			let mut cells_count = 0;
			let mut cell_type = self.contains_at(start_x, y);

			// Construit la ligne des groupes de cellules composés de leur décompte (optionnel s’il vaut 1) et de leur indicatif `b` ou `o`
			for x in start_x..(start_x + width) {
				if cell_type == self.contains_at(x, y) {
					cells_count += 1;
				}
				else {
					if cells_count == 1 {
						cells_grid.push(String::from(if cell_type { "o" } else { "b" }));
					}
					else {
						cells_grid.push(format!("{cells_count}{}", if cell_type { 'o' } else { 'b' }));
					}

					cells_count = 1;
					cell_type = !cell_type;
				}
			}

			// Et ajoute le dernier groupe s’il correspond à des cellules vivantes (optionnel si c’est groupe de cellules mortes)
			if cell_type {
				cells_grid.push(if cells_count == 1 { String::from("o") } else { format!("{cells_count}o") });
			}

			// Ajoute enfin le marqueur de fin de ligne ou de fin de fichier pour la dernière
			cells_grid.push(String::from(if y + 1 == start_y + height { "!" } else { "$" }));
		}

		// Insère les marqueurs de fin de ligne et de fin de fichier, puis casse le fichier en lignes de 70 caractères au plus (selon la norme)
		let mut line_start_index: usize = 0;
		let mut line_length: usize = 0;

		for (index, symbol) in cells_grid.iter().enumerate() {
			if line_length + symbol.len() > RLE_FILE_LINE_LENGTH {
				output_pattern.push_str(&(cells_grid[line_start_index..index].join("") + "\n"));
				line_start_index = index;
				line_length = 0;
			}

			// Dans tous les cas, ajoute la longueur du symbole courant au total
			line_length += symbol.len();
		}

		// Ajoute enfin la ligne non terminée
		output_pattern.push_str(&cells_grid[line_start_index..].join(""));
		output_pattern
	}

	// Méthodes nécessaires puisque l’accès direct aux méthodes de modification de Regle depuis JavaScript ne fonctionne pas
	pub fn add_birth_rule(&mut self, i: u8) {
		self.birth.add(i);
	}

	pub fn add_survival_rule(&mut self, i: u8) {
		self.survival.add(i);
	}

	pub fn toggle_birth_rule(&mut self, i: u8) {
		self.birth.toggle(i);
	}

	pub fn toggle_survival_rule(&mut self, i: u8) {
		self.survival.toggle(i);
	}

	pub fn empty_birth_rule(&mut self) {
		self.birth.empty();
	}

	pub fn empty_survival_rule(&mut self) {
		self.survival.empty();
	}

	#[must_use]
	pub fn get_bounds(&self) -> Bounds {
		let mut min_x = i32::MAX;
		let mut min_y = i32::MAX;
		let mut max_x = i32::MIN;
		let mut max_y = i32::MIN;

		for cell in &self.cells {
			min_x = min_x.min(cell.x);
			min_y = min_y.min(cell.y);
			max_x = max_x.max(cell.x);
			max_y = max_y.max(cell.y);
		}

		Bounds { min_x, min_y, max_x, max_y }
	}

	pub fn wipe(&mut self) {
		self.cells.clear();
		self.previous_dead_cells = [None, None, None, None, None, None, None, None];
		self.neighbourhoods.clear();
		self.generations = 0;
		self.population = 0;
	}

	pub fn reset(&mut self) {
		self.wipe();
		self.birth = Rule::new(&[3]);
		self.survival = Rule::new(&[2, 3]);
	}
}
