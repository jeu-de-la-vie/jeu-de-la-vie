import { fileURLToPath, URL } from "node:url"

import { svelte } from "@sveltejs/vite-plugin-svelte"
import { defineConfig } from "vitest/config"

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [svelte()],
	resolve: {
		alias: [
			{
				find: "src",
				replacement: fileURLToPath(new URL("./src", import.meta.url)),
			},
			{
				find: "pkg",
				replacement: fileURLToPath(new URL("./pkg", import.meta.url)),
			},
		],
	},
	test: {
		coverage: {
			include: [
				"src/",
			],
			exclude: [
				"src/vite-env.d.ts",
			],
			thresholds: {
				statements: 5,
				branches: 55,
				functions: 35,
				lines: 5,
			},
		},
	},
})
