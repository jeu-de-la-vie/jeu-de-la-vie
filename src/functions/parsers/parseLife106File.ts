export const LINE = /^ *(-?\d+) +(-?\d+)$/

const parseLife106File = (fileContent: string) => {
	const lines = fileContent.split("\n")
	const cells = new Set<[number, number]>()

	if (lines[0] !== "#Life 1.06") {
		throw new SyntaxError("Erreur dans le format du fichier : ligne d’entête manquante")
	}
	lines.shift()

	for (const [index, line] of lines.entries()) {
		if (line !== "") {
			if (!LINE.test(line)) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne malformée [ligne ${index + 1}]`)
			}

			const [_, x, y] = line.match(LINE)!
			cells.add([Number(x), Number(y)])
		}
	}

	return cells
}

export default parseLife106File
