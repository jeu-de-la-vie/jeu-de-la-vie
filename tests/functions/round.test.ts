import { expect, it } from "vitest"

import round from "src/functions/round"

it("rounds 1.234 to a specified number of decimal places", () => {
	const tests = [
		[0, 1],
		[1, 1.2],
		[2, 1.23],
		[3, 1.234],
		[4, 1.234],
	] as const

	for (const [precision, expectedValue] of tests) {
		expect(round(1.234, precision)).toBe(expectedValue)
	}
})

it("rounds 1.5678 to a specified number of decimal places", () => {
	const tests = [
		[0, 2],
		[1, 1.6],
		[2, 1.57],
		[3, 1.568],
		[4, 1.5678],
	] as const

	for (const [precision, expectedValue] of tests) {
		expect(round(1.5678, precision)).toBe(expectedValue)
	}
})
