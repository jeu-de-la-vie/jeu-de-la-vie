let wasm;

const cachedTextDecoder = (typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-8', { ignoreBOM: true, fatal: true }) : { decode: () => { throw Error('TextDecoder not available') } } );

if (typeof TextDecoder !== 'undefined') { cachedTextDecoder.decode(); };

let cachedUint8ArrayMemory0 = null;

function getUint8ArrayMemory0() {
    if (cachedUint8ArrayMemory0 === null || cachedUint8ArrayMemory0.byteLength === 0) {
        cachedUint8ArrayMemory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachedUint8ArrayMemory0;
}

function getStringFromWasm0(ptr, len) {
    ptr = ptr >>> 0;
    return cachedTextDecoder.decode(getUint8ArrayMemory0().subarray(ptr, ptr + len));
}

let WASM_VECTOR_LEN = 0;

const cachedTextEncoder = (typeof TextEncoder !== 'undefined' ? new TextEncoder('utf-8') : { encode: () => { throw Error('TextEncoder not available') } } );

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length, 1) >>> 0;
        getUint8ArrayMemory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len, 1) >>> 0;

    const mem = getUint8ArrayMemory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3, 1) >>> 0;
        const view = getUint8ArrayMemory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
        ptr = realloc(ptr, len, offset, 1) >>> 0;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

function isLikeNone(x) {
    return x === undefined || x === null;
}

let cachedDataViewMemory0 = null;

function getDataViewMemory0() {
    if (cachedDataViewMemory0 === null || cachedDataViewMemory0.buffer.detached === true || (cachedDataViewMemory0.buffer.detached === undefined && cachedDataViewMemory0.buffer !== wasm.memory.buffer)) {
        cachedDataViewMemory0 = new DataView(wasm.memory.buffer);
    }
    return cachedDataViewMemory0;
}

function _assertClass(instance, klass) {
    if (!(instance instanceof klass)) {
        throw new Error(`expected instance of ${klass.name}`);
    }
}

function addToExternrefTable0(obj) {
    const idx = wasm.__externref_table_alloc();
    wasm.__wbindgen_export_0.set(idx, obj);
    return idx;
}

function passArrayJsValueToWasm0(array, malloc) {
    const ptr = malloc(array.length * 4, 4) >>> 0;
    for (let i = 0; i < array.length; i++) {
        const add = addToExternrefTable0(array[i]);
        getDataViewMemory0().setUint32(ptr + 4 * i, add, true);
    }
    WASM_VECTOR_LEN = array.length;
    return ptr;
}

const BoundsFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_bounds_free(ptr >>> 0, 1));

export class Bounds {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Bounds.prototype);
        obj.__wbg_ptr = ptr;
        BoundsFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        BoundsFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_bounds_free(ptr, 0);
    }
    /**
     * @returns {number}
     */
    get min_x() {
        const ret = wasm.__wbg_get_bounds_min_x(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set min_x(arg0) {
        wasm.__wbg_set_bounds_min_x(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get min_y() {
        const ret = wasm.__wbg_get_bounds_min_y(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set min_y(arg0) {
        wasm.__wbg_set_bounds_min_y(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get max_x() {
        const ret = wasm.__wbg_get_bounds_max_x(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set max_x(arg0) {
        wasm.__wbg_set_bounds_max_x(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get max_y() {
        const ret = wasm.__wbg_get_bounds_max_y(this.__wbg_ptr);
        return ret;
    }
    /**
     * @param {number} arg0
     */
    set max_y(arg0) {
        wasm.__wbg_set_bounds_max_y(this.__wbg_ptr, arg0);
    }
}

const RuleFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_rule_free(ptr >>> 0, 1));

export class Rule {

    static __wrap(ptr) {
        ptr = ptr >>> 0;
        const obj = Object.create(Rule.prototype);
        obj.__wbg_ptr = ptr;
        RuleFinalization.register(obj, obj.__wbg_ptr, obj);
        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        RuleFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_rule_free(ptr, 0);
    }
    /**
     * @param {number} i
     * @returns {boolean}
     */
    contains(i) {
        const ret = wasm.rule_contains(this.__wbg_ptr, i);
        return ret !== 0;
    }
    /**
     * @param {number} i
     */
    add(i) {
        wasm.rule_add(this.__wbg_ptr, i);
    }
    /**
     * @param {number} i
     */
    remove(i) {
        wasm.rule_remove(this.__wbg_ptr, i);
    }
    /**
     * @param {number} i
     */
    toggle(i) {
        wasm.rule_toggle(this.__wbg_ptr, i);
    }
    empty() {
        wasm.rule_empty(this.__wbg_ptr);
    }
}

const SimulationFinalization = (typeof FinalizationRegistry === 'undefined')
    ? { register: () => {}, unregister: () => {} }
    : new FinalizationRegistry(ptr => wasm.__wbg_simulation_free(ptr >>> 0, 1));

export class Simulation {

    __destroy_into_raw() {
        const ptr = this.__wbg_ptr;
        this.__wbg_ptr = 0;
        SimulationFinalization.unregister(this);
        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_simulation_free(ptr, 0);
    }
    /**
     * @returns {Rule}
     */
    get birth() {
        const ret = wasm.__wbg_get_simulation_birth(this.__wbg_ptr);
        return Rule.__wrap(ret);
    }
    /**
     * @param {Rule} arg0
     */
    set birth(arg0) {
        _assertClass(arg0, Rule);
        var ptr0 = arg0.__destroy_into_raw();
        wasm.__wbg_set_simulation_birth(this.__wbg_ptr, ptr0);
    }
    /**
     * @returns {Rule}
     */
    get survival() {
        const ret = wasm.__wbg_get_simulation_survival(this.__wbg_ptr);
        return Rule.__wrap(ret);
    }
    /**
     * @param {Rule} arg0
     */
    set survival(arg0) {
        _assertClass(arg0, Rule);
        var ptr0 = arg0.__destroy_into_raw();
        wasm.__wbg_set_simulation_survival(this.__wbg_ptr, ptr0);
    }
    /**
     * @returns {number}
     */
    get generations() {
        const ret = wasm.__wbg_get_simulation_generations(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set generations(arg0) {
        wasm.__wbg_set_simulation_generations(this.__wbg_ptr, arg0);
    }
    /**
     * @returns {number}
     */
    get population() {
        const ret = wasm.__wbg_get_simulation_population(this.__wbg_ptr);
        return ret >>> 0;
    }
    /**
     * @param {number} arg0
     */
    set population(arg0) {
        wasm.__wbg_set_simulation_population(this.__wbg_ptr, arg0);
    }
    constructor() {
        const ret = wasm.simulation_new();
        this.__wbg_ptr = ret >>> 0;
        SimulationFinalization.register(this, this.__wbg_ptr, this);
        return this;
    }
    calculate_generation() {
        wasm.simulation_calculate_generation(this.__wbg_ptr);
    }
    /**
     * @param {number} x
     * @param {number} y
     * @returns {boolean}
     */
    contains_at(x, y) {
        const ret = wasm.simulation_contains_at(this.__wbg_ptr, x, y);
        return ret !== 0;
    }
    /**
     * @param {number} x
     * @param {number} y
     */
    create_at(x, y) {
        wasm.simulation_create_at(this.__wbg_ptr, x, y);
    }
    /**
     * @param {number} x
     * @param {number} y
     */
    delete_at(x, y) {
        wasm.simulation_delete_at(this.__wbg_ptr, x, y);
    }
    /**
     * @param {number} x
     * @param {number} y
     */
    toggle_at(x, y) {
        wasm.simulation_toggle_at(this.__wbg_ptr, x, y);
    }
    /**
     * @param {CanvasRenderingContext2D} canvas_context
     * @param {number} x_offset
     * @param {number} y_offset
     * @param {number} cell_size
     * @param {string[]} trace_colors
     */
    draw_trace(canvas_context, x_offset, y_offset, cell_size, trace_colors) {
        const ptr0 = passArrayJsValueToWasm0(trace_colors, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.simulation_draw_trace(this.__wbg_ptr, canvas_context, x_offset, y_offset, cell_size, ptr0, len0);
    }
    /**
     * @param {CanvasRenderingContext2D} canvas_context
     * @param {number} x_offset
     * @param {number} y_offset
     * @param {number} cell_size
     * @param {string} color
     */
    draw_cells(canvas_context, x_offset, y_offset, cell_size, color) {
        const ptr0 = passStringToWasm0(color, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.simulation_draw_cells(this.__wbg_ptr, canvas_context, x_offset, y_offset, cell_size, ptr0, len0);
    }
    /**
     * @param {string} pattern_name
     * @returns {string}
     */
    export_rle_format(pattern_name) {
        let deferred2_0;
        let deferred2_1;
        try {
            const ptr0 = passStringToWasm0(pattern_name, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
            const len0 = WASM_VECTOR_LEN;
            const ret = wasm.simulation_export_rle_format(this.__wbg_ptr, ptr0, len0);
            deferred2_0 = ret[0];
            deferred2_1 = ret[1];
            return getStringFromWasm0(ret[0], ret[1]);
        } finally {
            wasm.__wbindgen_free(deferred2_0, deferred2_1, 1);
        }
    }
    /**
     * @param {number} i
     */
    add_birth_rule(i) {
        wasm.simulation_add_birth_rule(this.__wbg_ptr, i);
    }
    /**
     * @param {number} i
     */
    add_survival_rule(i) {
        wasm.simulation_add_survival_rule(this.__wbg_ptr, i);
    }
    /**
     * @param {number} i
     */
    toggle_birth_rule(i) {
        wasm.simulation_toggle_birth_rule(this.__wbg_ptr, i);
    }
    /**
     * @param {number} i
     */
    toggle_survival_rule(i) {
        wasm.simulation_toggle_survival_rule(this.__wbg_ptr, i);
    }
    empty_birth_rule() {
        wasm.simulation_empty_birth_rule(this.__wbg_ptr);
    }
    empty_survival_rule() {
        wasm.simulation_empty_survival_rule(this.__wbg_ptr);
    }
    /**
     * @returns {Bounds}
     */
    get_bounds() {
        const ret = wasm.simulation_get_bounds(this.__wbg_ptr);
        return Bounds.__wrap(ret);
    }
    wipe() {
        wasm.simulation_wipe(this.__wbg_ptr);
    }
    reset() {
        wasm.simulation_reset(this.__wbg_ptr);
    }
}

async function __wbg_load(module, imports) {
    if (typeof Response === 'function' && module instanceof Response) {
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            try {
                return await WebAssembly.instantiateStreaming(module, imports);

            } catch (e) {
                if (module.headers.get('Content-Type') != 'application/wasm') {
                    console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve Wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);

                } else {
                    throw e;
                }
            }
        }

        const bytes = await module.arrayBuffer();
        return await WebAssembly.instantiate(bytes, imports);

    } else {
        const instance = await WebAssembly.instantiate(module, imports);

        if (instance instanceof WebAssembly.Instance) {
            return { instance, module };

        } else {
            return instance;
        }
    }
}

function __wbg_get_imports() {
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbg_fillRect_c38d5d56492a2368 = function(arg0, arg1, arg2, arg3, arg4) {
        arg0.fillRect(arg1, arg2, arg3, arg4);
    };
    imports.wbg.__wbg_setfillStyle_2205fca942c641ba = function(arg0, arg1, arg2) {
        arg0.fillStyle = getStringFromWasm0(arg1, arg2);
    };
    imports.wbg.__wbindgen_init_externref_table = function() {
        const table = wasm.__wbindgen_export_0;
        const offset = table.grow(4);
        table.set(0, undefined);
        table.set(offset + 0, undefined);
        table.set(offset + 1, null);
        table.set(offset + 2, true);
        table.set(offset + 3, false);
        ;
    };
    imports.wbg.__wbindgen_string_get = function(arg0, arg1) {
        const obj = arg1;
        const ret = typeof(obj) === 'string' ? obj : undefined;
        var ptr1 = isLikeNone(ret) ? 0 : passStringToWasm0(ret, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len1 = WASM_VECTOR_LEN;
        getDataViewMemory0().setInt32(arg0 + 4 * 1, len1, true);
        getDataViewMemory0().setInt32(arg0 + 4 * 0, ptr1, true);
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm0(arg0, arg1));
    };

    return imports;
}

function __wbg_init_memory(imports, memory) {

}

function __wbg_finalize_init(instance, module) {
    wasm = instance.exports;
    __wbg_init.__wbindgen_wasm_module = module;
    cachedDataViewMemory0 = null;
    cachedUint8ArrayMemory0 = null;


    wasm.__wbindgen_start();
    return wasm;
}

function initSync(module) {
    if (wasm !== undefined) return wasm;


    if (typeof module !== 'undefined') {
        if (Object.getPrototypeOf(module) === Object.prototype) {
            ({module} = module)
        } else {
            console.warn('using deprecated parameters for `initSync()`; pass a single object instead')
        }
    }

    const imports = __wbg_get_imports();

    __wbg_init_memory(imports);

    if (!(module instanceof WebAssembly.Module)) {
        module = new WebAssembly.Module(module);
    }

    const instance = new WebAssembly.Instance(module, imports);

    return __wbg_finalize_init(instance, module);
}

async function __wbg_init(module_or_path) {
    if (wasm !== undefined) return wasm;


    if (typeof module_or_path !== 'undefined') {
        if (Object.getPrototypeOf(module_or_path) === Object.prototype) {
            ({module_or_path} = module_or_path)
        } else {
            console.warn('using deprecated parameters for the initialization function; pass a single object instead')
        }
    }

    if (typeof module_or_path === 'undefined') {
        module_or_path = new URL('jeu_de_la_vie_bg.wasm', import.meta.url);
    }
    const imports = __wbg_get_imports();

    if (typeof module_or_path === 'string' || (typeof Request === 'function' && module_or_path instanceof Request) || (typeof URL === 'function' && module_or_path instanceof URL)) {
        module_or_path = fetch(module_or_path);
    }

    __wbg_init_memory(imports);

    const { instance, module } = await __wbg_load(await module_or_path, imports);

    return __wbg_finalize_init(instance, module);
}

export { initSync };
export default __wbg_init;
