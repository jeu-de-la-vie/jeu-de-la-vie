enum ScrollBehavior {
	Move,
	Zoom,
}

export default ScrollBehavior
