const round = (x: number, n: number) => {
	return Math.round(x * 10 ** n) / 10 ** n
}

export default round
