FROM rust:1-alpine AS wasm-builder
RUN apk add --no-cache make musl-dev
RUN adduser -D wasm
USER wasm
WORKDIR /home/wasm
RUN wget -q -O - https://rustwasm.github.io/wasm-pack/installer/init.sh | sh
# Compile les dépendences
COPY ./Makefile ./Cargo.toml ./Cargo.lock ./
RUN mkdir -p src/rust \
 && echo "fn main() {}" > src/rust/lib.rs \
 && make wasm \
 && rm -rf target/wasm32*/release/.fingerprint/jeu-de-la-vie*
# Puis compile le projet
COPY ./src/rust ./src/rust
RUN make wasm

FROM node:22-alpine AS builder
USER node
WORKDIR /home/node
COPY package.json package-lock.json ./
RUN npm ci
COPY --from=wasm-builder /home/wasm/pkg ./pkg
COPY ./ ./
RUN npm run build

FROM busybox:1-musl AS client
RUN adduser -D static
USER static
WORKDIR /home/static
RUN echo ".wasm:application/wasm" > httpd.conf
COPY --from=builder /home/node/dist .
CMD ["busybox", "httpd", "-f", "-v", "-p", "8080"]
