enum PlayStrategy {
	Scheduled,
	Unbridled,
}

export default PlayStrategy
