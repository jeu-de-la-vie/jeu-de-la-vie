enum EditMode {
	Create,
	Toggle,
	Delete,
}

export default EditMode
