enum Modal {
	ApplicationHelp,
	FileError,
	FileReceiptReady,
}

export default Modal
