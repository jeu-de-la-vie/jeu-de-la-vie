const NAME_LINE = /^#N +([^ ].+)$/
const COMMENT_LINE = /^#C +([^ ].+)$/
const AUTHOR_LINE = /^#O +([^ ].+)$/
const DEFINITION_LINE = /^x ?= ?(\d+), y ?= ?(\d+)(?:, rule ?= ?[Bb](\d*)\/[Ss](\d*))?$/
const HEADER_LINE = /^#[A-Z] +[^ ].+$/

const CELLS_LINE = /(?:\d*[bo$])*/
const FINAL_CELLS_LINE = /(?:\d*[bo$])*!/

const parseRleFile = (fileContent: string) => {
	// Traiter fichier RLE
	const lines = fileContent.split("\n")

	// Implémentation stricte du format Run Length Encoded
	// #N = nom du modèle
	// #O = auteur du modèle
	// #C = commentaire
	// Pas d’autre entête spécifique toléré (#P, #R ou #r)
	let name: string | undefined
	let author: string | undefined
	const comments: Array<string> = []
	let startX: number | undefined
	let startY: number | undefined
	let birthRules: Set<number> | undefined
	let survivalRules: Set<number> | undefined
	const cells = new Set<[number, number]>()
	let flagEndHeaders = false
	let flagEndFile = false
	let x: number | undefined
	let y: number | undefined

	for (const [index, line] of lines.entries()) {
		if (line === "") {
			continue
		}

		else if (NAME_LINE.test(line)) {
			// Si le nom du modèle est déjà défini, le format est erroné (une seule ligne #N autorisée)
			if (name !== undefined || flagEndHeaders) {
				throw new SyntaxError(`Erreur dans le format du fichier : une seule ligne d’entête de nom de modèle autorisée [ligne ${index + 1}]`)
			}

			else if (flagEndHeaders) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne d’entête (nom) rencontrée après la ligne d’entête [ligne ${index + 1}]`)
			}

			name = line.match(NAME_LINE)![1]
		}

		else if (AUTHOR_LINE.test(line)) {
			// Si l’auteur du modèle est déjà défini, le format est erroné (une seule ligne #O autorisée)
			if (author !== undefined) {
				throw new SyntaxError(`Erreur dans le format du fichier : une seule ligne d’entête d’auteur autorisée [ligne ${index + 1}]`)
			}

			else if (flagEndHeaders) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne d’entête (auteur) rencontrée après la ligne de définition [ligne ${index + 1}]`)
			}

			author = line.match(AUTHOR_LINE)![1]
		}

		else if (COMMENT_LINE.test(line)) {
			if (flagEndHeaders) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne d’entête (commentaire) rencontrée après la ligne de définition [ligne ${index + 1}]`)
			}

			comments.push(line.match(COMMENT_LINE)![1])
		}

		else if (HEADER_LINE.test(line)) {
			if (flagEndHeaders) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne d’entête non supporté rencontrée après la ligne de définition [ligne ${index + 1}]`)
			}

			throw new SyntaxError(`Erreur dans le format du fichier : ligne d’entête non supporté [ligne ${index + 1}]`)
		}

		else if (DEFINITION_LINE.test(line)) {
			if (cells.size > 0) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne de définition rencontrée après la première ligne de cellules [ligne ${index + 1}]`)
			}

			// Si les coordonnées de départ du modèle sont déjà définies, le format est erroné (une seule ligne de définition autorisée)
			else if (x !== undefined || y !== undefined) {
				throw new SyntaxError(`Erreur dans le format du fichier : plusieurs lignes de définition rencontrées [ligne ${index + 1}]`)
			}

			// Récupère les valeurs dans la chaine
			const [_, width, height, birthValues, survivalValues] = line.match(DEFINITION_LINE)!

			// Et les attribue aux variables
			startX = -Math.ceil(Number(width) / 2)
			startY = -Math.ceil(Number(height) / 2)
			x = startX
			y = startY

			if (birthValues !== undefined && survivalValues !== undefined) {
				birthRules = new Set(birthValues.split("").map(Number))
				survivalRules = new Set(survivalValues.split("").map(Number))
			}

			flagEndHeaders = true
		}

		else if (CELLS_LINE.test(line) || FINAL_CELLS_LINE.test(line)) {
			if (x === undefined || y === undefined) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne de définition manquante [ligne ${index + 1}]`)
			}

			else if (flagEndFile) {
				throw new SyntaxError(`Erreur dans le format du fichier : ligne de cellules rencontrée après la fin du fichier [ligne ${index + 1}]`)
			}

			let count = ""
			let type: string | undefined
			for (const character of line.split("")) {
				if (/\d/.test(character)) {
					count += character
				}

				else if (/[bo]/.test(character)) {
					type = character

					for (let i = 0; i < Number(count || 1); i += 1) {
						if (type === "o") {
							cells.add([x, y])
						}

						x += 1
					}

					count = ""
					type = undefined
				}

				else if (character === "$") {
					x = startX!
					y += Number(count || 1)
					count = ""
				}

				else if (character === "!") {
					flagEndFile = true
				}

				else {
					throw new SyntaxError(`Erreur dans le format du fichier : ligne de cellules malformée [ligne ${index + 1}]`)
				}
			}
		}

		else {
			throw new SyntaxError(`Erreur dans le format du fichier : ligne malformée [ligne ${index + 1}]`)
		}
	}

	return {
		name,
		author,
		comments,
		cells,
		birthRules,
		survivalRules,
	}
}

export default parseRleFile
