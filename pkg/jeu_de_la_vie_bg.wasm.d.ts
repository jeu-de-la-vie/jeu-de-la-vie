/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export const __wbg_rule_free: (a: number, b: number) => void;
export const rule_contains: (a: number, b: number) => number;
export const rule_add: (a: number, b: number) => void;
export const rule_remove: (a: number, b: number) => void;
export const rule_toggle: (a: number, b: number) => void;
export const rule_empty: (a: number) => void;
export const __wbg_bounds_free: (a: number, b: number) => void;
export const __wbg_get_bounds_min_x: (a: number) => number;
export const __wbg_set_bounds_min_x: (a: number, b: number) => void;
export const __wbg_get_bounds_min_y: (a: number) => number;
export const __wbg_set_bounds_min_y: (a: number, b: number) => void;
export const __wbg_get_bounds_max_x: (a: number) => number;
export const __wbg_set_bounds_max_x: (a: number, b: number) => void;
export const __wbg_get_bounds_max_y: (a: number) => number;
export const __wbg_set_bounds_max_y: (a: number, b: number) => void;
export const __wbg_simulation_free: (a: number, b: number) => void;
export const __wbg_get_simulation_birth: (a: number) => number;
export const __wbg_set_simulation_birth: (a: number, b: number) => void;
export const __wbg_get_simulation_survival: (a: number) => number;
export const __wbg_set_simulation_survival: (a: number, b: number) => void;
export const __wbg_get_simulation_generations: (a: number) => number;
export const __wbg_set_simulation_generations: (a: number, b: number) => void;
export const __wbg_get_simulation_population: (a: number) => number;
export const __wbg_set_simulation_population: (a: number, b: number) => void;
export const simulation_new: () => number;
export const simulation_calculate_generation: (a: number) => void;
export const simulation_contains_at: (a: number, b: number, c: number) => number;
export const simulation_create_at: (a: number, b: number, c: number) => void;
export const simulation_delete_at: (a: number, b: number, c: number) => void;
export const simulation_toggle_at: (a: number, b: number, c: number) => void;
export const simulation_draw_trace: (a: number, b: any, c: number, d: number, e: number, f: number, g: number) => void;
export const simulation_draw_cells: (a: number, b: any, c: number, d: number, e: number, f: number, g: number) => void;
export const simulation_export_rle_format: (a: number, b: number, c: number) => [number, number];
export const simulation_add_birth_rule: (a: number, b: number) => void;
export const simulation_add_survival_rule: (a: number, b: number) => void;
export const simulation_toggle_birth_rule: (a: number, b: number) => void;
export const simulation_toggle_survival_rule: (a: number, b: number) => void;
export const simulation_empty_birth_rule: (a: number) => void;
export const simulation_empty_survival_rule: (a: number) => void;
export const simulation_get_bounds: (a: number) => number;
export const simulation_wipe: (a: number) => void;
export const simulation_reset: (a: number) => void;
export const __wbindgen_export_0: WebAssembly.Table;
export const __wbindgen_malloc: (a: number, b: number) => number;
export const __wbindgen_realloc: (a: number, b: number, c: number, d: number) => number;
export const __externref_table_alloc: () => number;
export const __wbindgen_free: (a: number, b: number, c: number) => void;
export const __wbindgen_start: () => void;
