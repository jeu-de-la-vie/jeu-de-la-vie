// Permet de ne pas excéder une certaine fréquence lors d’appels répétés à une fonction
// Le délai indique le temps minimal laissé entre deux appels
const debounce = (callback: CallableFunction, delay: number) => {
	let timer: number | undefined

	return async (...args: Array<unknown>) => {
		clearTimeout(timer)

		// Permet de récupérer le résultat de la fonction de rappel
		return await new Promise((resolve) => {
			timer = setTimeout(() => {
				resolve(callback(...args))
			}, delay)
		})
	}
}

export default debounce
