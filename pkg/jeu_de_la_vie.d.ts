/* tslint:disable */
/* eslint-disable */
export class Bounds {
  private constructor();
  free(): void;
  min_x: number;
  min_y: number;
  max_x: number;
  max_y: number;
}
export class Rule {
  private constructor();
  free(): void;
  contains(i: number): boolean;
  add(i: number): void;
  remove(i: number): void;
  toggle(i: number): void;
  empty(): void;
}
export class Simulation {
  free(): void;
  constructor();
  calculate_generation(): void;
  contains_at(x: number, y: number): boolean;
  create_at(x: number, y: number): void;
  delete_at(x: number, y: number): void;
  toggle_at(x: number, y: number): void;
  draw_trace(canvas_context: CanvasRenderingContext2D, x_offset: number, y_offset: number, cell_size: number, trace_colors: string[]): void;
  draw_cells(canvas_context: CanvasRenderingContext2D, x_offset: number, y_offset: number, cell_size: number, color: string): void;
  export_rle_format(pattern_name: string): string;
  add_birth_rule(i: number): void;
  add_survival_rule(i: number): void;
  toggle_birth_rule(i: number): void;
  toggle_survival_rule(i: number): void;
  empty_birth_rule(): void;
  empty_survival_rule(): void;
  get_bounds(): Bounds;
  wipe(): void;
  reset(): void;
  birth: Rule;
  survival: Rule;
  generations: number;
  population: number;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_rule_free: (a: number, b: number) => void;
  readonly rule_contains: (a: number, b: number) => number;
  readonly rule_add: (a: number, b: number) => void;
  readonly rule_remove: (a: number, b: number) => void;
  readonly rule_toggle: (a: number, b: number) => void;
  readonly rule_empty: (a: number) => void;
  readonly __wbg_bounds_free: (a: number, b: number) => void;
  readonly __wbg_get_bounds_min_x: (a: number) => number;
  readonly __wbg_set_bounds_min_x: (a: number, b: number) => void;
  readonly __wbg_get_bounds_min_y: (a: number) => number;
  readonly __wbg_set_bounds_min_y: (a: number, b: number) => void;
  readonly __wbg_get_bounds_max_x: (a: number) => number;
  readonly __wbg_set_bounds_max_x: (a: number, b: number) => void;
  readonly __wbg_get_bounds_max_y: (a: number) => number;
  readonly __wbg_set_bounds_max_y: (a: number, b: number) => void;
  readonly __wbg_simulation_free: (a: number, b: number) => void;
  readonly __wbg_get_simulation_birth: (a: number) => number;
  readonly __wbg_set_simulation_birth: (a: number, b: number) => void;
  readonly __wbg_get_simulation_survival: (a: number) => number;
  readonly __wbg_set_simulation_survival: (a: number, b: number) => void;
  readonly __wbg_get_simulation_generations: (a: number) => number;
  readonly __wbg_set_simulation_generations: (a: number, b: number) => void;
  readonly __wbg_get_simulation_population: (a: number) => number;
  readonly __wbg_set_simulation_population: (a: number, b: number) => void;
  readonly simulation_new: () => number;
  readonly simulation_calculate_generation: (a: number) => void;
  readonly simulation_contains_at: (a: number, b: number, c: number) => number;
  readonly simulation_create_at: (a: number, b: number, c: number) => void;
  readonly simulation_delete_at: (a: number, b: number, c: number) => void;
  readonly simulation_toggle_at: (a: number, b: number, c: number) => void;
  readonly simulation_draw_trace: (a: number, b: any, c: number, d: number, e: number, f: number, g: number) => void;
  readonly simulation_draw_cells: (a: number, b: any, c: number, d: number, e: number, f: number, g: number) => void;
  readonly simulation_export_rle_format: (a: number, b: number, c: number) => [number, number];
  readonly simulation_add_birth_rule: (a: number, b: number) => void;
  readonly simulation_add_survival_rule: (a: number, b: number) => void;
  readonly simulation_toggle_birth_rule: (a: number, b: number) => void;
  readonly simulation_toggle_survival_rule: (a: number, b: number) => void;
  readonly simulation_empty_birth_rule: (a: number) => void;
  readonly simulation_empty_survival_rule: (a: number) => void;
  readonly simulation_get_bounds: (a: number) => number;
  readonly simulation_wipe: (a: number) => void;
  readonly simulation_reset: (a: number) => void;
  readonly __wbindgen_export_0: WebAssembly.Table;
  readonly __wbindgen_malloc: (a: number, b: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number, d: number) => number;
  readonly __externref_table_alloc: () => number;
  readonly __wbindgen_free: (a: number, b: number, c: number) => void;
  readonly __wbindgen_start: () => void;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {{ module: SyncInitInput }} module - Passing `SyncInitInput` directly is deprecated.
*
* @returns {InitOutput}
*/
export function initSync(module: { module: SyncInitInput } | SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {{ module_or_path: InitInput | Promise<InitInput> }} module_or_path - Passing `InitInput` directly is deprecated.
*
* @returns {Promise<InitOutput>}
*/
export default function __wbg_init (module_or_path?: { module_or_path: InitInput | Promise<InitInput> } | InitInput | Promise<InitInput>): Promise<InitOutput>;
