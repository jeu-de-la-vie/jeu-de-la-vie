import { expect, it } from "vitest"

import formatFileSize from "src/functions/formatFileSize"

it("formats 512 bytes", () => {
	const tests = [
		[{ mode: "binary", locale: "fr-FR", minimumFractionDigits: 0 }, "512 o"],
		[{ mode: "binary", locale: "fr-FR", minimumFractionDigits: 2 }, "512 o"],

		[{ mode: "binary", locale: "en-US", minimumFractionDigits: 0 }, "512 o"],
		[{ mode: "binary", locale: "en-US", minimumFractionDigits: 2 }, "512 o"],

		[{ mode: "binary-lower-unit", locale: "fr-FR", minimumFractionDigits: 0 }, "512 o"],
		[{ mode: "binary-lower-unit", locale: "fr-FR", minimumFractionDigits: 2 }, "512 o"],

		[{ mode: "binary-lower-unit", locale: "en-US", minimumFractionDigits: 0 }, "512 o"],
		[{ mode: "binary-lower-unit", locale: "en-US", minimumFractionDigits: 2 }, "512 o"],

		[{ mode: "decimal", locale: "fr-FR", minimumFractionDigits: 0 }, "512 o"],
		[{ mode: "decimal", locale: "fr-FR", minimumFractionDigits: 2 }, "512 o"],

		[{ mode: "decimal", locale: "en-US", minimumFractionDigits: 0 }, "512 o"],
		[{ mode: "decimal", locale: "en-US", minimumFractionDigits: 2 }, "512 o"],
	] as const

	for (const [options, expectedValue] of tests) {
		expect(formatFileSize(512, options)).toBe(expectedValue)
	}
})

it("formats 1_000 bytes", () => {
	const tests = [
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "0,98 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "0,98 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "0,98 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "0,980 Kio"],
		[{ mode: "binary", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "0,977 Kio"],

		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "0.98 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "0.98 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "0.98 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "0.980 Kio"],
		[{ mode: "binary", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "0.977 Kio"],

		[{ mode: "binary-lower-unit", locale: "fr-FR", minimumFractionDigits: 0 }, "1 000 o"],
		[{ mode: "binary-lower-unit", locale: "fr-FR", minimumFractionDigits: 2 }, "1 000 o"],

		[{ mode: "binary-lower-unit", locale: "en-US", minimumFractionDigits: 0 }, "1,000 o"],
		[{ mode: "binary-lower-unit", locale: "en-US", minimumFractionDigits: 2 }, "1,000 o"],

		[{ mode: "decimal", locale: "fr-FR", minimumFractionDigits: 0 }, "1 Ko"],
		[{ mode: "decimal", locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Ko"],
		[{ mode: "decimal", locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Ko"],
		[{ mode: "decimal", locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Ko"],

		[{ mode: "decimal", locale: "en-US", minimumFractionDigits: 0 }, "1 Ko"],
		[{ mode: "decimal", locale: "en-US", minimumFractionDigits: 1 }, "1.0 Ko"],
		[{ mode: "decimal", locale: "en-US", minimumFractionDigits: 2 }, "1.00 Ko"],
		[{ mode: "decimal", locale: "en-US", minimumFractionDigits: 3 }, "1.000 Ko"],
	] as const

	for (const [options, expectedValue] of tests) {
		expect(formatFileSize(1_000, options)).toBe(expectedValue)
	}
})

it("formats 1_024 bytes", () => {
	const tests = [
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Kio"],
		[{ mode: "binary", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Kio"],

		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Kio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Kio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Kio"],
		[{ mode: "binary", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Kio"],

		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Kio"],

		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Kio"],

		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "1,02 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 0 }, "1,024 Ko"],
		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "1,02 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 1 }, "1,024 Ko"],
		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "1,02 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 2 }, "1,024 Ko"],
		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "1,020 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "1,024 Ko"],

		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "1.02 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 0 }, "1.024 Ko"],
		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "1.02 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 1 }, "1.024 Ko"],
		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "1.02 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 2 }, "1.024 Ko"],
		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Ko"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "1.020 Ko"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "1.024 Ko"],
	] as const

	for (const [options, expectedValue] of tests) {
		expect(formatFileSize(1_024, options)).toBe(expectedValue)
	}
})

it("formats 1_024_144 bytes", () => {
	const tests = [
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "0,98 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 0 }, "0,977 Mio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "0,98 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 1 }, "0,977 Mio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "0,98 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 2 }, "0,977 Mio"],
		[{ mode: "binary", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "0,980 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "0,977 Mio"],

		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "0.98 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 0 }, "0.977 Mio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "0.98 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 1 }, "0.977 Mio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "0.98 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 2 }, "0.977 Mio"],
		[{ mode: "binary", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Mio"],
		[{ mode: "binary", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "0.980 Mio"],
		[{ mode: "binary", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "0.977 Mio"],

		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 000,1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "1 000,14 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1 000,1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "1 000,14 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1 000,10 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "1 000,14 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "1 000,140 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "1 000,141 Kio"],

		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1,000.1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "1,000.14 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1,000.1 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "1,000.14 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1,000.10 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "1,000.14 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "1,000.140 Kio"],
		[{ mode: "binary-lower-unit", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "1,000.141 Kio"],

		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 0 }, "1 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 0 }, "1,02 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 0 }, "1,024 Mo"],
		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 1 }, "1,0 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 1 }, "1,02 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 1 }, "1,024 Mo"],
		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 2 }, "1,00 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 2 }, "1,02 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 2 }, "1,024 Mo"],
		[{ mode: "decimal", roundPrecision: 1, locale: "fr-FR", minimumFractionDigits: 3 }, "1,000 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "fr-FR", minimumFractionDigits: 3 }, "1,020 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "fr-FR", minimumFractionDigits: 3 }, "1,024 Mo"],

		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 0 }, "1 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 0 }, "1.02 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 0 }, "1.024 Mo"],
		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 1 }, "1.0 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 1 }, "1.02 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 1 }, "1.024 Mo"],
		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 2 }, "1.00 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 2 }, "1.02 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 2 }, "1.024 Mo"],
		[{ mode: "decimal", roundPrecision: 1, locale: "en-US", minimumFractionDigits: 3 }, "1.000 Mo"],
		[{ mode: "decimal", roundPrecision: 2, locale: "en-US", minimumFractionDigits: 3 }, "1.020 Mo"],
		[{ mode: "decimal", roundPrecision: 3, locale: "en-US", minimumFractionDigits: 3 }, "1.024 Mo"],
	] as const

	for (const [options, expectedValue] of tests) {
		expect(formatFileSize(1_024_144, options)).toBe(expectedValue)
	}
})
