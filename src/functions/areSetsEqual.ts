const areSetsEqual = <T>(a: Set<T>, b: Set<T>) => {
	if (a === b) {
		return true
	}

	// Set.values() returns an Iterator
	// But Iterator.every has been supported for barely a year now
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Iterator/every#browser_compatibility
	// Users of browsers that have recently become obsolete will encounter an error
	// So we need to use an Array.from wrapper
	// The same applies to the latest Set methods, which is why we do not use them
	return a.size === b.size && Array.from(a.values()).every((value) => b.has(value))
}

export default areSetsEqual
