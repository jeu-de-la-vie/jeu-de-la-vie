import antfu from "@antfu/eslint-config"

export default antfu(
	{
		typescript: true,
		svelte: true,
		unicorn: true,

		stylistic: {
			indent: "tab",
			quotes: "double",
			semi: false,
		},

		ignores: [
			// Fichiers autogénérés par wasm-pack
			"pkg",
		],
	},
	// Désactive ou configure des règles prédéfinies
	{
		rules: {
			// Préfère les fonctions fléchées partout, avec parenthèses obligatoires
			"antfu/top-level-function": "off",
			"style/arrow-parens": ["error", "always"],
			// Désactive la validation de l’ordre des clés, beaucoup trop vétilleuse
			"jsonc/sort-keys": "off",
			// Désactive le formatage des tableaux dans TOML
			"toml/array-bracket-spacing": "off",
			"toml/array-element-newline": "off",
			// Autorise les accès de propriétés par indice (notation `object[index]`)
			"dot-notation": "off",
			// Autorise les espaces insécables à usage typographique
			"no-irregular-whitespace": "off",
			// Personnalise le tri des imports
			"perfectionist/sort-imports": [
				"error",
				{
					customGroups: {
						value: {
							src: "^src/[^\/]+$",
							components: "^src/components/.+$",
							functions: "^src/functions/.+$",
							enums: "^src/enums/.+$",
							pkg: "^pkg/.+$",
						},
						type: {
							src: "^src/[^\/]+$",
							components: "^src/components/.+$",
							functions: "^src/functions/.+$",
							enums: "^src/enums/.+$",
							pkg: "^pkg/.+$",
						},
					},
					groups: [
						"builtin",
						"external-type",
						"external",
						"src",
						"enums",
						"functions",
						"components",
						"pkg",
					],
					newlinesBetween: "always",
					order: "asc",
				},
			],
		},
	},
	// Ajoute quelques règles importantes de préférences personnelles
	{
		rules: {
			// Cf. https://eslint.org/docs/latest/rules/
			"curly": ["error", "all"],
			"no-plusplus": "error",
			// Interdit les imports locaux (., ./ and ../), utiliser plutôt les alias src/ et pkg/
			"no-restricted-imports": [
				"error",
				{
					patterns: ["\.", "\./*", "\.\./*"],
				},
			],
			"no-shadow": "error",
		},
	},
)
